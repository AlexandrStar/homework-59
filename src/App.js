import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './components/ToDo/AddTaskForm';
import Task from "./components/ToDo/ToDo";

class App extends Component {

    state = {
        tasks: [],
        textInput: '',
    };

    addTask = () => {
        if (this.state.textInput !== '') {
            const tasks =  this.state.tasks;
            const newTask = {
                text: this.state.textInput,
                id: Date.now()
            };

            tasks.push(newTask);

            this.setState({tasks});
        }
    };

    removeItems = id => {
        const tasks = this.state.tasks;
        const index = tasks.findIndex(task => {
            return (
                task.id === id
            )
        });
        tasks.splice(index, 1);

        this.setState({tasks})
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };

    changeFilmName = (event, index) => {
      let tasks = this.state.tasks;
      tasks[index].text = event.target.value;
      this.setState({tasks});
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm
                     change={(event) => this.changeHandler(event)}
                     add={() => this.addTask()}
                />
                <div className='tasks'>
                  <p>To watch list:</p>
                  {this.state.tasks.map((task, id) => (
                    <Task
                      key={id}
                      value={task.text}
                      remove = {() => this.removeItems(id)}
                      onChange={task => this.changeFilmName(task, id)}
                    />
                  ))}

                </div>
            </div>
        );
    }
}

export default App;
