import React from 'react';
import './ToDo.css';

const AddTaskForm = (props) => {
        return (
            <div>
            <div className='task-form'>
                <input onChange={props.change} className='task-input' type="text" />
                <button onClick={props.add} className='btn-add'>Add</button>
            </div>
            </div>
        )
};


export default AddTaskForm;