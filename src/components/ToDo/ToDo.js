import React, {Component} from 'react';
import './ToDo.css';



class Task extends Component {

  shouldComponentUpdate(nextProps) {
    console.log('[Post] ShouldUpdate');
    return nextProps.value !== this.props.value;
  }

  render() {
    console.log('[Post] render');
    return (
      <div
        className='task'
        key={this.props.id}>
        <input
          className="filmName"
          value={this.props.value}
          onChange={this.props.onChange}/>
        <button
          onClick={this.props.remove}
          className='btn-remove'
        >Remove</button>
      </div>
    )
  }

}

export default Task;